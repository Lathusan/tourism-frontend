import React from 'react'
import Banner from './Banner'

const BookTrip = () => {
    return (
        <div className="container" style={{ marginBlockStart: '-90px' }}>
            {/* <Banner/> */}
            <section className="trip-area section_gap">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7 col-md-10">
                            <div className="trip-form-section">
                                <h1>Book a Trip <br/> for your next Tour </h1>
                                <form
                                    className="trip-form-area trip-page-form trip-form text-right"
                                    id="myForm"
                                    action="mail.html"
                                    method="post"
                                >
                                    <div className="form-group col-md-12">
                                        <div className="form-select">
                                            <select style={{ display: "none" }}>
                                                <option value={1}>Select Packages</option>
                                                <option value={1}>Package 01</option>
                                                <option value={1}>Package 02</option>
                                                <option value={1}>Package 03</option>
                                                <option value={1}>Package 04</option>
                                            </select>
                                            <div className="nice-select" tabIndex={0}>
                                                <span className="current">Select Packages</span>
                                                <ul className="list">
                                                    <li data-value={1} className="option selected">
                                                        Select Packages
                                </li>
                                                    <li data-value={1} className="option">
                                                        Package 01
                                </li>
                                                    <li data-value={1} className="option">
                                                        Package 02
                                </li>
                                                    <li data-value={1} className="option">
                                                        Package 03
                                </li>
                                                    <li data-value={1} className="option">
                                                        Package 04
                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group col-md-12">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="name"
                                            name="name"
                                            placeholder="To"
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'To'"
                                        />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="From"
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'From'"
                                        />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="subject"
                                            name="subject"
                                            placeholder="Return"
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Return'"
                                        />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="subject"
                                            name="subject"
                                            placeholder="Adults"
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Adults'"
                                        />
                                    </div>
                                    <div className="form-group col-md-12">
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="subject"
                                            name="subject"
                                            placeholder="Child"
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Child'"
                                        />
                                    </div>
                                    <div className="col-lg-12 text-center">
                                        <button className="primary-btn text-uppercase">
                                            Search Flights
                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>;

        </div>
    )
}

export default BookTrip
