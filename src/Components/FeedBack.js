import React from 'react'

const FeedBack = () => {
    return (
        <div className="container">
            <div class="ol-lg-12 ">
                <div class="main_title">

                    <h1>FeedBack</h1>
                    <span class="title-widget-bg"></span>
                </div>
            </div>
            <div class="ol-lg-12 ">
                <div class="quotes">
                    MCSE boot camps have its supporters and its detractors. Some people do not understand
                    why you should have to spend money on boot camp when you can get the MCSE study
                    materials yourself at a fraction of the camp price. However, who has the willpower to
                    actually sit through a self-imposed MCSE training.
                    </div>
            </div><br></br>
            <div className="col-lg-12 text-center container ">
                <form
                    className="row contact_form"
                    action="contact_process.php"
                    method="post"
                    id="contactForm"
                    noValidate
                >
                    <div className="col-md-6">
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                id="name"
                                name="name"
                                placeholder="Enter your name"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="email"
                                className="form-control"
                                id="email"
                                name="email"
                                placeholder="Enter email address"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                id="subject"
                                name="subject"
                                placeholder="Enter Subject"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                id="phone"
                                name="phone"
                                placeholder="Enter Phone Number"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                id="bookingid"
                                name="bookingid"
                                placeholder="Enter Booking Id"
                            />
                        </div>
                        <div className="form-group">
                            <textarea
                                className="form-control"
                                name="message"
                                id="message"
                                rows={1}
                                placeholder="Enter Message"
                                defaultValue={""}
                            />
                        </div>
                        <button
                            type="submit"
                            value="submit"
                            className="primary-btn text-uppercase"
                        >
                            Send Message
                    </button>
                    </div>
                </form>
            </div>;
        </div>
    )
}

export default FeedBack
