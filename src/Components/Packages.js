import React from 'react';
import Banner from './Banner';


function NavigationBar() {
    return (
        <>
            {/* <Banner/> */}
            <div className="package-area section_gap_top container" style={{marginBlockStart:'-90px'}}>
                {/* section 1 */}
                <div style={{ backgroundColor: '#efdaf5' }}>
                    <div class="row d-flex justify-content-center">
                        <div class="ol-lg-12">
                            <div class="main_title">
                                <p>We’re Offering these Trip Packages</p>
                                <h1>Famous Trips Packages</h1>
                                <span class="title-widget-bg"></span>
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{ display: "flex" }}>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                                {/* <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a> */}
                            </div>

                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                                {/* <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Top worl heritages<br />
                    and details</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a> */}
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                                {/* <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p> */}

                            </div>
                            <a href="#" className="primary-btn" style={{ marginInlineStart: '250px' }}>Read More</a>
                        </div>
                    </div>
                </div>

                <br/><br/><br/><br/>

                <div style={{ backgroundColor: '#efdaf5' }}>
                    <div class="row d-flex justify-content-center">
                        <div class="ol-lg-12">
                            <div class="main_title">
                                <p>We’re Offering these Trip Packages</p>
                                <h1>Famous Trips Packages</h1>
                                <span class="title-widget-bg"></span>
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{ display: "flex" }}>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                            <a href="#" className="primary-btn" style={{ marginInlineStart: '250px' }}>Read More</a>
                        </div>
                    </div>
                </div>

                <br/><br/><br/><br/>

                <div style={{ backgroundColor: '#efdaf5' }}>
                    <div class="row d-flex justify-content-center">
                        <div class="ol-lg-12">
                            <div class="main_title">
                                <p>We’re Offering these Trip Packages</p>
                                <h1>Famous Trips Packages</h1>
                                <span class="title-widget-bg"></span>
                            </div>
                        </div>
                    </div>
                    <div className="row" style={{ display: "flex" }}>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6" style={{}}>
                            <div className="single-package">
                                <div className="thumb">
                                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt="" />
                                </div>
                            </div>
                            <a href="#" className="primary-btn" style={{ marginInlineStart: '250px' }}>Read More</a>
                        </div>
                    </div>
                </div>


                {/* section 2
                <div className="row align-items-end justify-content-left">
                    <div className="col-lg-12">
                        <div className="main_title">
                            <p></p>
                            <h1>Top World heritages and details </h1>
                            <span className="title-widget-bg" />
                        </div>
                    </div>
                </div>
                <div className="row"> */}
                    {/* single-feature */}
                    {/* <div className="col-lg-4 col-md-6">
                        <div className="single-feature">
                            <div className="feature-details">
                                <h5>
                                    Desert Riding Turning <br />
                        So much Flowery
                    </h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do
                                    eiusmod tempor incididunt.
                    </p>
                                <a href="#" className="primary-btn mb-40">
                                    Read More
                    </a>
                            </div>
                            <div className="feature-thumb">
                                <img
                                    className="img-fluid"
                                    src={require('../img/card-img.jpg')}
                                    alt
                                    data-pagespeed-url-hash={395841825}
                                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                />
                            </div>
                        </div>
                    </div> */}
                    {/* single-feature */}
                    {/* <div className="col-lg-4 col-md-6">
                        <div className="single-feature">
                            <div className="feature-details">
                                <h5>
                                    Relaxation in the <br />
                        Local Beach Campfire
                    </h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do
                                    eiusmod tempor incididunt.
                    </p>
                                <a href="#" className="primary-btn mb-40">
                                    Read More
                    </a>
                            </div>
                            <div className="feature-thumb">
                                <img
                                    className="img-fluid"
                                    src={require('../img/card-img.jpg')}
                                    alt
                                    data-pagespeed-url-hash={690341746}
                                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                />
                            </div>
                        </div>
                    </div> */}
                    {/* single-feature */}
                    {/* <div className="col-lg-4 col-md-6">
                        <div className="single-feature">
                            <div className="feature-details">
                                <h5>
                                    Forest Exploration <br />
                        with Energy Package
                    </h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do
                                    eiusmod tempor incididunt.
                    </p>
                                <a href="#" className="primary-btn mb-40">
                                    Read More
                    </a>
                            </div>
                            <div className="feature-thumb">
                                <img
                                    className="img-fluid"
                                    src={require('../img/card-img.jpg')}
                                    alt
                                    data-pagespeed-url-hash={984841667}
                                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                />
                            </div>
                        </div>
                    </div>
                </div> */}

                {/* <div className="row d-flex justify-content-center">
                    <div className="col-lg-12">
                        <div className="main_title">

                            <h1>Popular Places All Around Sri Lanka</h1>
                            <span className="title-widget-bg" />
                        </div>
                    </div>
                </div> */}

            </div>

            {/* <section className="popular-places-area section_gap_bottom">
                <div className="popular-places-slider owl-carousel owl-loaded owl-drag">
                    <div className="owl-stage-outer">
                        <div
                            className="owl-stage"
                            style={{
                                transform: "translate3d(-1053px, 0px, 0px)",
                                transition: "all 0s ease 0s",
                                width: 4212
                            }}
                        >
                            <div className="owl-item cloned" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={2142410963}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>All Over destinations in  Sri Lanka </p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={2436910884}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item active" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={1553411121}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item active" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={1847911042}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={2142410963}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={2436910884}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={1553411121}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour</p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-item cloned" style={{ width: "526.5px" }}>
                                <div className="single-popular-places">
                                    <div className="popular-places-img">
                                        <img
                                            src={require('../img/card-img.jpg')}
                                            alt
                                            data-pagespeed-url-hash={1847911042}
                                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                        />
                                    </div>
                                    <div className="popular-places-text">
                                        <a href="single-blog.html"> </a>
                                        <p>Proper Guided Tour  </p>
                                        <h4>Santorini Island Dream Holiday and Fun package</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="owl-nav disabled">
                        <div className="owl-prev">prev</div>
                        <div className="owl-next">next</div>
                    </div>
                    <div className="owl-dots disabled" />
                    <div className="owl-thumbs" />
                </div>
            </section> */}

        </>


    );
}
export default NavigationBar;