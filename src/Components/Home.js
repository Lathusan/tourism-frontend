import React from 'react';

function Home() {
    return (
        <div style={{ width: '100%' }}>
            <section className="home_banner_area">
                <div className="banner_inner">
                    <div className="container">
                        <div className="row fullscreen d-flex align-items-center justify-content-center">
                            <div className="banner_content">
                                <h2>Lanka Tourism</h2>
                                <input type="text" placeholder="Search.." style={{ height: '50px', width: '500px', borderRadius: '30px', marginBlockStart: '80px' }}></input>
                                <button style={{ height: '50px', width: '80px', borderRadius: '30px' }}>Go</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="feature-area section_gap_top" style={{ height: '960px' }}>
                <div className="container">
                    <div className="row align-items-end justify-content-left">
                        <div className="col-lg-12">
                            <div className="main_title">
                                <h3>This webapp has the best online tourism management system with user friendly, enriched functionaly and versatile platform.</h3>
                                <span className="title-widget-bg" />
                            </div>
                        </div>
                    </div>
                    <h1 style={{ textAlign: 'center', color: 'purple' }}><i>Top destinations</i></h1>
                    <div className="row" style={{ marginBlockStart: '70px' }}>
                        <div className="col-lg-4 col-md-6">
                            <div className="single-feature">
                                <div className="feature-details">
                                    <h5>Desert Riding Turning <br />So much Flowery</h5>
                                    <p>Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                    <a href="#" className="primary-btn mb-40">Read More</a>
                                </div>
                                <div className="feature-thumb">
                                    <img className="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQLceblZSNXjfci8xwoR2ZFXMJw0l6-E_P9mA&usqp=CAU" alt="" style={{ height: '150px', width: '230px' }} />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div className="single-feature">
                                <div className="feature-details">
                                    <h5>Relaxation in the <br />Local Beach Campfire</h5>
                                    <p>Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                    <a href="#" className="primary-btn mb-40">Read More</a>
                                </div>
                                <div className="feature-thumb">
                                    <img className="img-fluid" src="https://thebeachclub.spectrumresorts.com/wp-content/uploads/2018/12/campfires_beach_club_resort_gulf_shores_alabama5.jpg" alt="" style={{ height: '150px', width: '230px' }} />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div className="single-feature">
                                <div className="feature-details">
                                    <h5>Forest Exploration <br />with Energy Package</h5>
                                    <p>Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.</p>
                                    <a href="#" className="primary-btn mb-40">Read More</a>
                                </div>
                                <div className="feature-thumb">
                                    <img className="img-fluid" src="https://srilankaecotourism.lk/images/activity_img/rain-forest-exploration/11.jpg" alt="" style={{ height: '150px', width: '230px' }} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div className="main_title" style={{ marginBlockEnd: '100px' }}>
                <h1>Benefits</h1>
                <span className="title-widget-bg" />
                <div style={{ width: '720px', textAlign: 'left', margin: '0 auto', fontSize: '17px' }}>
                    <p>&#9672; Saves time and money.</p><br />
                    <p>&#9672; Offer real time data access.</p><br />
                    <p>&#9672; Minimize your maintenance cost.</p><br />
                    <p>&#9672; 24*7 interactions with customers.</p><br />
                    <p>&#9672; Minimized order processing costs.</p><br />
                    <p>&#9672; Helpful in expansion of your business.</p><br />
                    <p>&#9672; Instant reservation or booking facilities of flights, tour packages etc.</p>
                </div>
            </div>

            <div className="container" style={{ textAlign: 'center', marginBlockEnd: '100px' }}>
                <h2 style={{ marginBlockEnd: '100px' }}>Explore more attractions</h2>
                <div style={{ height: '300px', width: '300px', backgroundColor: '#bbb', borderRadius: '50%', display: 'inline-block' }} >
                    <img className="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ_VDiYr9m_wkXL3hgZKtbm6mRCGVioX0VY2g&usqp=CAU" alt="" style={{ height: '300px', width: '300px,', borderRadius: '50%' }} />
                </div>
                <div style={{ height: '300px', width: '300px', backgroundColor: '#bbb', borderRadius: '50%', display: 'inline-block', marginInlineStart: '80px' }} >
                    <img className="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRmaP2Nv2OisYIbOo_CnPqm_8ku0SISBRuwSw&usqp=CAU" alt="" style={{ height: '300px', width: '300px,', borderRadius: '50%' }} />
                </div>
                <div style={{ height: '300px', width: '300px', backgroundColor: '#bbb', borderRadius: '50%', display: 'inline-block', marginInlineStart: '80px' }} >
                    <img className="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRbO1fAHM--F4jlxFc8Swv_ggF8JsDVRBZNkA&usqp=CAU" alt="" style={{ height: '300px', width: '300px,', borderRadius: '50%' }} />
                </div>
            </div>

        </div>
    );
}
export default Home;