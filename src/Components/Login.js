import React from 'react'

const Login = () => {
    return (
        <>
            <section class="contact_area section_gap container">
                <div class="container d-flex justify-content-center">
                    <div className="row" style={{margin:'center', backgroundColor:'magenta', height:'350px', width:'500px'}}>
                        <div className="ol-lg-12" style={{height:'100px'}}>
                            <div className="main_title">
                                <h1>Log In Form</h1>
                                <span className="title-widget-bg" />
                            </div>
                        </div>
                        <div className="col-md-9" style={{marginBlockEnd:'80px'}}>
                            <form
                                className="row contact_form"
                                action="contact_process.php"
                                method="post"
                                id="contactForm"
                                noValidate
                            >
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="name"
                                        name="name"
                                        placeholder="Enter User name"
                                    /><br />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="name"
                                        name="name"
                                        placeholder="Enter Password"
                                    />
                                </div>
                                <div className="col-md-9 ">
                                    <button
                                        type="submit"
                                        value="submit"
                                        className="primary-btn text-uppercase"
                                    >
                                        Log In
                                </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Login;
