import React from 'react'

const Register = () => {
    return (
        <>
            <div className="section_gap_top container" style={{marginBlockStert:'-100px'}}>
                {/* section 1 */}
                <div class="row d-flex justify-content-center">
                    <div class="ol-lg-12" >
                        <div class="main_title" >
                            <p>We’re Offering these Trip Packages</p>
                            <h1>Famous Trips Packages</h1>
                            <span class="title-widget-bg"></span>
                        </div>
                    </div>
                </div>
                <div class=" justify-content-left">
                    <div class="ol-lg-12">
                        <form action="#">
                            <div className="mt-10">
                                <input
                                    type="text"
                                    name="first_name"
                                    placeholder="First Name"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'First Name'"
                                    required
                                    className="single-input"
                                />
                            </div>
                            <div className="mt-10">
                                <input
                                    type="text"
                                    name="last_name"
                                    placeholder="Last Name"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Last Name'"
                                    required
                                    className="single-input"
                                />
                            </div>
                            <div className="mt-10">
                                <input
                                    type="text"
                                    name="last_name"
                                    placeholder="Last Name"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Last Name'"
                                    required
                                    className="single-input"
                                />
                            </div>
                            <div className="mt-10">
                                <input
                                    type="email"
                                    name="EMAIL"
                                    placeholder="Email address"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Email address'"
                                    required
                                    className="single-input"
                                />
                            </div>
                            <div className="input-group-icon mt-10">
                                <div className="icon">
                                    <i className="fa fa-thumb-tack" aria-hidden="true" />
                                </div>
                                <input
                                    type="text"
                                    name="address"
                                    placeholder="Address"
                                    onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = 'Address'"
                                    required
                                    className="single-input"
                                />
                            </div>
                            <div className="input-group-icon mt-10">
                                <div className="icon">
                                    <i className="fa fa-plane" aria-hidden="true" />
                                </div>
                                <div className="form-select" id="default-select">
                                    <select style={{ display: "none" }}>
                                        <option value={1}>City</option>
                                        <option value={1}>Dhaka</option>
                                        <option value={1}>Dilli</option>
                                        <option value={1}>Newyork</option>
                                        <option value={1}>Islamabad</option>
                                    </select>
                                    <div className="nice-select" tabIndex={0}>
                                        <span className="current">City</span>
                                        <ul className="list">
                                            <li data-value={1} className="option selected">
                                                City
                                    </li>
                                            <li data-value={1} className="option">
                                                Dhaka
                                    </li>
                                            <li data-value={1} className="option">
                                                Dilli
                                    </li>
                                            <li data-value={1} className="option">
                                                Newyork
                                    </li>
                                            <li data-value={1} className="option">
                                                Islamabad
                                    </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="input-group-icon mt-10">
                                <div className="icon">
                                    <i className="fa fa-globe" aria-hidden="true" />
                                </div>
                                <div className="form-select" id="default-select2">
                                    <select style={{ display: "none" }}>
                                        <option value={1}>Country</option>
                                        <option value={1}>Bangladesh</option>
                                        <option value={1}>India</option>
                                        <option value={1}>England</option>
                                        <option value={1}>Srilanka</option>
                                    </select>
                                    <div className="nice-select" tabIndex={0}>
                                        <span className="current">Country</span>
                                        <ul className="list">
                                            <li data-value={1} className="option selected">
                                                Country
                                    </li>
                                            <li data-value={1} className="option">
                                                Bangladesh
                                    </li>
                                            <li data-value={1} className="option">
                                                India
                                    </li>
                                            <li data-value={1} className="option">
                                                England
                                    </li>
                                            <li data-value={1} className="option">
                                                Srilanka
                                    </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-12 text-center">

                                <button className="primary-btn text-uppercase">
                                    Clear
                                </button>
                                <button className="primary-btn text-uppercase">
                                    Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <br/> <br/> <br/> <br/>
            </div>
        
        </>
    )
}

export default Register;
