import React from 'react'

const AboutUs = () => {
    return (
        <>
            <section className="section_gap about-area container" style={{marginBlockStart:'-90px'}}>
                <div className="container">
                    <div className="single-about row">
                        <div className="col-lg-4 col-md-6 no-padding about-left">
                            <div className="about-content">
                                <h1>
                                    We Believe <br />
                            that Interior beauty <br />
                            Lasts Long
                        </h1>
                                <p>
                                    inappropriate behavior is often laughed off as “boys will be boys,”
                                    women face higher conduct standards especially in the workplace.
                                    That’s why it’s crucial that as women. Lorem ipsum dolor sit amet,
                                    consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua.
                        </p>
                            </div>
                        </div>
                        <div className="col-lg-8 col-md-6 text-center no-padding about-right">
                            <div className="about-thumb">
                                <img
                                    src={require('../img/about-img1.jpg')}
                                    className="img-fluid info-img"
                                    alt
                                    data-pagespeed-url-hash={1742008268}
                                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {/* sec 2 */}
            <div className="cta-area section_gap container">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-5">
                            <h1>Get Ready for Real time Adventure</h1>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                                minim veniam quis nostrud exercitation.
                        </p>
                            <a href="#" className="primary-btn">
                                Book a Trip
                        </a>
                        </div>
                        <div className="offset-lg-1 col-lg-6">
                            <img
                                className="cta-img img-fluid"
                                src={require('../img/about-sec2.png')}
                                alt
                                data-pagespeed-url-hash={411448515}
                                onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AboutUs;
