import React from 'react'

const Tours = () => {
    return (
        <>
             <div style={{}} className="package-area section_gap_top container">
            {/* section 1 */}
                <div class="row d-flex justify-content-center">
                    <div class="ol-lg-12">
                    <div class="main_title">
                    
                    <h1>Tours | Sri Lanka</h1>
                    <span class="title-widget-bg"></span>
                    </div>
                    </div>
                </div>
                <div className="row"  style={{display:"flex"}}>
            <div className="col-lg-4 col-md-6" style={{}}>
                <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
                
            </div>
            <div className="col-lg-4 col-md-6" style={{}}>
            <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Top worl heritages<br />
                    and details</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
            </div>
            <div className="col-lg-4 col-md-6" style={{}}>
            <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
            </div>  
            </div> 

            <div class="row d-flex justify-content-center">
                    <div class="ol-lg-12">
                    <div class="main_title">
                    
                    <h1>Tours | World</h1>
                    <span class="title-widget-bg"></span>
                    </div>
                    </div>
                </div>
                <div className="row"  style={{display:"flex"}}>
            <div className="col-lg-4 col-md-6" style={{}}>
                <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
                
            </div>
            <div className="col-lg-4 col-md-6" style={{}}>
            <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Top worl heritages<br />
                    and details</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
            </div>
            <div className="col-lg-4 col-md-6" style={{}}>
            <div className="single-package">
                    <div className="thumb">
                    <img className="img-fluid" src={require('../img/card-img.jpg')} alt=""  />
                    </div>
                    <div className="meta-top d-flex">
                    <p><span className="fa fa-location-arrow" /> Stockholmes</p>
                    <p className="ml-20"><span className="fa fa-calendar" /> 5 days 6 nights</p>
                    </div>
                    <h4>Desert Riding Turning So <br />
                    much Flowery</h4>
                    <p>
                    Lorem ipsum dolor sit amet, consecter adipisicing elit, sed do eiusmod tempor incididunt.
                    </p>
                    <a href="#" className="primary-btn">Read More</a>
                </div>
            </div>  
            </div> 
            </div>
        </>
    )
}

export default Tours
